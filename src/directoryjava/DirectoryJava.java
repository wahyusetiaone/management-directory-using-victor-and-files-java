/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package directoryjava;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.LinkedList;
import java.util.Vector;

/**
 *
 * @author abah
 */
public class DirectoryJava {
    static Vector<String> valueDirectory = new Vector<String>();
    static Vector<String> fileJPG = new Vector<String>();
    static Vector<String> filePDF = new Vector<String>();
    static Vector<String> fileTXT = new Vector<String>();
    static Vector<String> filePPT = new Vector<String>();
    static String[] extentionFile = {"jpg","pdf","txt","ppt"};
    static String path;
    public static void main(String[] args) throws IOException{
      path = "/home/abah/Desktop/TestProgram";
      readDirectory();
      readExtention();
      readFoundFile();
      createDirectoryForFile();
      moveToNewDirectory();
  }

    private static void readDirectory() {
        System.out.println("Reading Directory ......");
        // directory
        File aDirectory = new File(path);

        //save to Vector
        String[] value = aDirectory.list();
        for (int x = 0; x < value.length; x++){
            valueDirectory.add(x, value[x]);
        }
    }

    private static void readExtention() {
        System.out.println("Reading Extention ......");

        for(int i = 0; i < extentionFile.length;i++){
            
            String search  = extentionFile[i];
            for(int x = 0; x < valueDirectory.size();x++){
                String sentence = valueDirectory.get(x);
                Path file = new File(path+"/"+sentence).toPath();
                if(!Files.isDirectory(file)){
                    if ( sentence.toLowerCase().indexOf(search.toLowerCase()) != -1 ) {

                        switch(i) {
                            case 0:
                                fileJPG.add(valueDirectory.get(x));
                                break;
                            case 1:
                                filePDF.add(valueDirectory.get(x));
                                break;
                            case 2:
                                fileTXT.add(valueDirectory.get(x));
                                break;
                            case 3:
                                filePPT.add(valueDirectory.get(x));
                                break;
                        }

                     }
                }
                
            }
        }
    }

    private static void readFoundFile() {
        System.out.println("File Found : ");
        System.out.println("-------------------------------------------------");
        for(int i = 0; i < extentionFile.length;i++){
            System.out.print("|| "+extentionFile[i].toUpperCase()+" ");
            switch(i) {
                case 0:
                   System.out.print(fileJPG.size()+" || ");
                   break;
                case 1:
                   System.out.print(filePDF.size()+" || ");
                   break;
                case 2:
                   System.out.print(fileTXT.size()+" || ");
                   break;
                case 3:
                   System.out.print(filePPT.size()+" || ");
                   break;
            }
        }
        System.out.println();        
        System.out.println("-------------------------------------------------");

    }

    private static void createDirectoryForFile() {
        for(int i = 0; i < extentionFile.length;i++){
            File theDir = new File(path +"/"+extentionFile[i].toUpperCase());

            // if the directory does not exist, create it
            if (!theDir.exists()) {
                System.out.println("creating directory: " + theDir.getName());
                boolean result = false;

                try{
                    theDir.mkdir();
                    result = true;
                } 
                catch(SecurityException se){
                    //handle it
                }        
                if(result) {    
                    System.out.println("DIR created");  
                }
            }
        }
    }

    private static void moveToNewDirectory() throws IOException {
        File destinationFolder;
        File sourceFolder = new File(path);

        // Check weather source exists and it is folder.
        if (sourceFolder.exists())
        {
            System.out.println("Moving Files ......");
            //JPG
            if (fileJPG != null){
                System.out.println("  JPG Files ......");
                destinationFolder = new File(path +"/JPG");
               if(destinationFolder.exists()){
                    for (String fileString : fileJPG ){
                        
                    // Move files to destination folder
                    File sourceFile = new File(sourceFolder+"/"+fileString);
                    File destinationFile = new File(destinationFolder+"/"+fileString);
                    try {
                        Files.move(sourceFile.toPath(), destinationFile.toPath(),StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception e) {
                        //moving file failed.
                        e.printStackTrace();
                    }
                }
               }else{
                   System.out.println(destinationFolder + "  Folder does not exists");
               }
            }
            //PDF
            if (filePDF != null){
                System.out.println("  PDF Files ......");
                destinationFolder = new File(path +"/PDF");
               if(destinationFolder.exists()){
                    for (String fileString : filePDF ){
                        
                    // Move files to destination folder
                    File sourceFile = new File(sourceFolder+"/"+fileString);
                    File destinationFile = new File(destinationFolder+"/"+fileString);
                    try {
                        Files.move(sourceFile.toPath(), destinationFile.toPath(),StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception e) {
                        //moving file failed.
                        e.printStackTrace();
                    }
                }
               }else{
                   System.out.println(destinationFolder + "  Folder does not exists");
               }
            }
            //TXT
            if (fileTXT != null){
                System.out.println("  TXT Files ......");
                destinationFolder = new File(path +"/TXT");
               if(destinationFolder.exists()){
                    for (String fileString : fileTXT ){
                        
                    // Move files to destination folder
                    File sourceFile = new File(sourceFolder+"/"+fileString);
                    File destinationFile = new File(destinationFolder+"/"+fileString);
                    try {
                        Files.move(sourceFile.toPath(), destinationFile.toPath(),StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception e) {
                        //moving file failed.
                        e.printStackTrace();
                    }
                }
               }else{
                   System.out.println(destinationFolder + "  Folder does not exists");
               }
            }
            //PPT
            if (filePPT != null){
                System.out.println("  PPT Files ......");
                destinationFolder = new File(path +"/PPT");
               if(destinationFolder.exists()){
                    for (String fileString : filePPT ){
                        
                    // Move files to destination folder
                    File sourceFile = new File(sourceFolder+"/"+fileString);
                    File destinationFile = new File(destinationFolder+"/"+fileString);
                    try {
                        Files.move(sourceFile.toPath(), destinationFile.toPath(),StandardCopyOption.ATOMIC_MOVE);
                    } catch (Exception e) {
                        //moving file failed.
                        e.printStackTrace();
                    }
                }
               }else{
                   System.out.println(destinationFolder + "  Folder does not exists");
               }
            }
        }
        else
        {
            System.out.println(sourceFolder + "  Folder does not exists");
        }
    }
    
}
