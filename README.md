# Management Directory Using Victor and Files Java

A simple program java to management your directory

## Getting Started

To run this program you should have JRE and Netbeans 8.2 or latest

### Prerequisites

To install Netbeans and JRE you can follow official site 
* [Netbeans](https://netbeans.org/)

### Installing

Download program and open in your Netbeans.
then you can develop it!

## Running Program

First insert your directory

```
/home/user/yourdirectory
```

### Formats Files

Specific format files change this line.

```
static String[] extentionFile = {"jpg","pdf","txt","ppt"};
```


## Built With

* import java.io.File;
* import java.io.IOException;
* import java.nio.file.Files;
* import java.nio.file.Path;
* import java.nio.file.Paths;
* import java.nio.file.StandardCopyOption;
* import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
* import java.util.LinkedList;
* import java.util.Vector;